var site = {
    settings: {
        debug: false
    },
    init: function(settings){
        $.extend(this.settings, settings);
    },

};

$(function(){
    site.init({
        debug: true
    });

    // smoothScroll init
    smoothScroll.init({
        offset: 90
    });
});
