
# BEM

## Property order overview

Try to respect this order. This way it'll be easier and faster to find certain properties.

```
.selector {
    //includes and extends
    //positioning
    //box-model
    //border
    //visual
    //typography
    //colors
    //misc (like transforms, opacity)
    //states
    //pseudo elements
    //modifiers
    //children
    //media queries
}
```

## Property order

```
.selector { // space after selector ;)
    @include mixin(variable);
    @extend %placeholder;

    content: '';

    /* Positioning */
    position: absolute;
    top: 0;
    left: 0;
    z-index: 10; // keep values clean, for example don't start with 9999

    /* Box-model */
    display: block;
    padding: 2rem; // body fontsize = 10px
    margin: 0; // Avoid specifying units for zero values, e.g., margin: 0; instead of margin: 0px;
    width: 50%;
    overflow: hidden;

    /* Border */
    border: .1rem solid darken(tomato, 5%);
    border-radius: 4rem;

    /* Visual */
    background: tomato;
    box-shadow: 0 0 1rem rgba(0, 0, 0, .25); // Don't prefix property values or color parameters with a leading zero (e.g., .5 instead of 0.5 and -.5px instead of -0.5px)

    /* Typography */
    line-height: 1.4; //only use units when it's absolutely necessary f.i. centering something
    font: { // nest properties from the same "group"
        family: $font-family--base;
        size: 1rem;
    }
    color: $base-color;
    vertical-align: top;
    text {
        align: center;
        decoration: underline;
        transform: uppercase;
    }

    /* Misc */
    transform: translate3d(-50%, -50%, 0); // try to use 3d transforms when possible
    transition: color .3s ease-in-out;
    will-change: color;
    outline: none;
    list-style: none;
    backface-visibility: hidden;
    white-space: nowrap;
    opacity: .5;

    /* States */
    &:hover, // enters elements in selector list
    &:focus {
        color: $grey--darker; // is darker than --dark ;)
    }

    /* Pseudo elements */
    &:after {
        @include mixin(variable);
        @extend %placeholder;

        content: ''; // content is placed between extend/includes and positioning

        position: absolute;
    }

    /* Modifiers */
    &--modifier { // instead of &.selector--modifier
        background: lime;
    }

    /* Children */
    // Children & & + &

    /* Media queries */
    @media (min-width: $screen-md) { // try to use pre defined breakpoints in variables, don't create a bunch of specific breakpoints like min-width: 574px, 874px etc
        width: 100%;
        padding: 3rem;
    }
}
```
